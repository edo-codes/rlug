const app = new Vue({
  el: "#app",
  template: "#app-template",
  data: () => ({
    loading: 0,
    inited: false,
    signedin: false,
    user: "",
    mailinglijsten: [],
    selectedmailinglijst: null,
    searchtext: "",
    modalaction: null,
    modalcontent: "",
    selectedemail: "",
    mailinglijstenbyemail: [],
    lastmessage: ""
  }),
  mounted() {
    console.log("mounted");
    gapi.load("client:auth2", async () => {
      await gapi.client.init({
        apiKey: "AIzaSyA1NNSZqzGQTcLRWN7r6VipDVInmuJ3plU",
        discoveryDocs: [
          "https://www.googleapis.com/discovery/v1/apis/admin/directory_v1/rest",
          "https://www.googleapis.com/discovery/v1/apis/admin/reports_v1/rest"
        ],
        clientId:
          "276867738473-clhhr3bv7utq920c6p9s26u46gko6jl7.apps.googleusercontent.com",
        scope: [
          "https://www.googleapis.com/auth/admin.directory.group",
          "https://www.googleapis.com/auth/admin.directory.group.member"
        ].join(" ")
      });
      const auth = gapi.auth2.getAuthInstance();
      this.signedin = auth.isSignedIn.get();
      if (this.signedin)
        this.user = auth.currentUser
          .get()
          .getBasicProfile()
          .getEmail();
      this.inited = true;
      if (this.signedin) this.loadMailinglijsten();
      history.pushState(null, "", "#");
      window.onpopstate = () => {
        this.selectedmailinglijst = event.state;
        $(".modal").modal("hide");
      };
    });
  },
  updated() { },
  computed: {
    searchmailinglijsten() {
      if (this.searchtext.length === 0) return this.mailinglijsten;
      const r = new RegExp(this.searchtext, "i");
      return this.mailinglijsten.filter(
        m => r.test(m.Naam) || r.test(m.Emailadres)
      );
    },
    splitmodalcontent() {
      return this.modalcontent
        .split(/[,\n ]/m)
        .map(l => l.trim())
        .filter(l => l.length > 0);
    }
  },
  methods: {
    async signin() {
      const auth = gapi.auth2.getAuthInstance();
      await auth.signIn({
        prompt: "select_account"
      });
      this.signedin = auth.isSignedIn.get();
      this.user = auth.currentUser
        .get()
        .getBasicProfile()
        .getEmail();
      auth.isSignedIn.listen(value => {
        this.signedin = value;
      });
      auth.currentUser.listen(googleUser => {
        this.user = googleUser.getBasicProfile().getEmail();
      });
      this.loadMailinglijsten();
    },
    async signout() {
      const auth = gapi.auth2.getAuthInstance();
      await auth.signOut();
      this.signedin = auth.isSignedIn.get();
    },
    async loadMailinglijsten() {
      this.loading++;
      this.mailinglijsten.splice(0, this.mailinglijsten.length);
      let response = null;
      let nextPageToken = null;
      do {
        response = await gapi.client.directory.groups.list({
          customer: "my_customer",
          fields: "groups(email,name),nextPageToken",
          pageToken: nextPageToken
        });
        console.log(response);
        if (response.result.groups)
          this.mailinglijsten.push(
            ...response.result.groups.map(({ email, name }) => ({
              Emailadres: email,
              Naam: name,
              Leden: []
            }))
          );
      } while (
        response.result.nextPageToken &&
        (nextPageToken = response.result.nextPageToken)
      );
      this.loading--;
    },
    selectmailinglijst(mailinglijst) {
      $("#byemailmodal").modal("hide");
      this.selectedmailinglijst = mailinglijst;
      this.loadmailinglijstleden();
      history.pushState(
        mailinglijst,
        mailinglijst.Emailadres,
        "#" +
        encodeURIComponent(mailinglijst.Emailadres.replace("@biton.nl", ""))
      );
    },
    async loadmailinglijstleden() {
      if (!this.selectedmailinglijst) return;
      this.loading++;
      this.selectedmailinglijst.Leden.splice(
        0,
        this.selectedmailinglijst.Leden.length
      );
      let response = null;
      let nextPageToken = null;
      do {
        response = await gapi.client.request({
          path: `https://www.googleapis.com/admin/directory/v1/groups/${encodeURIComponent(
            this.selectedmailinglijst.Emailadres
          )}/members`,
          params: {
            fields: "members/email,nextPageToken",
            pageToken: nextPageToken
          }
        });
        console.log(response);
        if (response.result.members)
          this.selectedmailinglijst.Leden.push(
            ...response.result.members.map(({ email }) => email)
          );
      } while (
        response.result.nextPageToken &&
        (nextPageToken = response.result.nextPageToken)
      );
      this.loading--;
    },
    async removelid(mailinglijst, lid) {
      if (
        !confirm(
          `Weet je zeker dat je ${lid} wilt verwijderen uit ${mailinglijst.Emailadres}?`
        )
      )
        return;
      this.loading++;
      response = await gapi.client.directory.members.delete({
        customer: "my_customer",
        groupKey: mailinglijst.Emailadres,
        memberKey: lid
      });
      console.log(response);
      this.loadmailinglijstleden();
      this.loading--;
    },
    ledentoevoegen() {
      this.modalaction = "toevoegen";
      this.modalcontent = "";
      setTimeout(() => {
        $("#batchmodal")
          .modal("setting", "duration", 50)
          .modal("setting", "observeChanges", true)
          .modal({ inverted: true })
          .modal("show");
      }, 0);
      history.pushState(
        this.selectedmailinglijst,
        this.selectedmailinglijst.Emailadres,
        "#" +
        encodeURIComponent(
          this.selectedmailinglijst.Emailadres.replace("@biton.nl", "")
        )
      );
    },
    ledenverwijderen() {
      this.modalaction = "verwijderen";
      this.modalcontent = "";
      setTimeout(() => {
        $("#batchmodal")
          .modal("setting", "duration", 50)
          .modal("setting", "observeChanges", true)
          .modal({ inverted: true })
          .modal("show");
      }, 0);
      history.pushState(
        this.selectedmailinglijst,
        this.selectedmailinglijst.Emailadres,
        "#" +
        encodeURIComponent(
          this.selectedmailinglijst.Emailadres.replace("@biton.nl", "")
        )
      );
    },
    modalcancel() {
      $("#batchmodal").modal("hide");
      this.modalaction = null;
      this.modalcontent = "";
    },
    async execledentoevoegen() {
      if (
        confirm(
          `Weet je zeker dat je deze ${this.splitmodalcontent.length.toString()} leden wilt toevoegen aan ${
          this.selectedmailinglijst.Naam
          }?`
        )
      ) {
        this.loading++;
        const batch = gapi.client.newBatch();
        const mailinglijst = this.selectedmailinglijst.Emailadres;
        for (let email of this.splitmodalcontent) {
          batch.add(
            gapi.client.request({
              path: `https://www.googleapis.com/admin/directory/v1/groups/${encodeURIComponent(
                this.selectedmailinglijst.Emailadres
              )}/members`,
              method: "POST",
              body: {
                email: email
              }
            })
          );
        }
        const result = await batch;
        console.log(result);
        const dispresult = [];
        for (let e of Object.values(result.result).entries()) {
          const a = this.splitmodalcontent[e[0]] + ": ";
          if (e[1].status && e[1].status === 200)
            dispresult.push(a + "Toegevoegd");
          else if (
            e[1].result &&
            e[1].result.error &&
            e[1].result.error.message
          )
            dispresult.push(a + e[1].result.error.message);
          else if (e[1].body)
            dispresult.push(a + JSON.stringify(JSON.parse(e[1].body)));
          else dispresult.push(a + JSON.stringify(e[1]));
        }
        this.lastmessage = dispresult.join("\n");
        this.modalcancel();
        this.loadmailinglijstleden();
        this.loading--;
      }
    },
    async execledenverwijderen() {
      if (
        confirm(
          `Weet je zeker dat je deze ${this.splitmodalcontent.length.toString()} leden wilt verwijderen uit ${
          this.selectedmailinglijst.Naam
          }?`
        )
      ) {
        this.loading++;
        const batch = gapi.client.newBatch();
        const mailinglijst = this.selectedmailinglijst.Emailadres;
        for (let email of this.splitmodalcontent) {
          batch.add(
            gapi.client.directory.members.delete({
              groupKey: mailinglijst,
              memberKey: email
            })
          );
        }
        const response = await batch;
        console.log(response);
        const dispresult = [];
        for (let e of Object.values(response.result).entries()) {
          const a = this.splitmodalcontent[e[0]] + ": ";
          if (e[1].status && e[1].status === 200)
            dispresult.push(a + "Verwijderd");
          else if (
            e[1].result &&
            e[1].result.error &&
            e[1].result.error.message
          )
            dispresult.push(a + e[1].result.error.message);
          else if (e[1].body)
            dispresult.push(a + JSON.stringify(JSON.parse(e[1].body)));
          else dispresult.push(a + JSON.stringify(e[1]));
        }
        this.lastmessage = dispresult.join("\n");
        this.modalcancel();
        this.loadmailinglijstleden();
        this.loading--;
      }
    },
    goback() {
      this.selectedmailinglijst = null;
      history.pushState(null, "", "#");
    },
    togglefullscreen() {
      const requestfs = () =>
        (
          document.documentElement.requestFullscreen ||
          document.documentElement.mozRequestFullScreen ||
          document.documentElement.webkitRequestFullscreen ||
          document.documentElement.msRequestFullscreen
        ).call(document.documentElement);
      const elementfs = () =>
        document.fullscreenElement ||
        document.mozFullscreenElement ||
        document.webkitFullscreenElement ||
        document.msFullscreenElement;
      const exitfs = () =>
        (
          document.exitFullscreen ||
          document.mozCancelFullScreen ||
          document.webkitExitFullscreen ||
          document.msExitFullscreen
        ).call(document);
      if (elementfs()) exitfs();
      else requestfs();
    },
    async loadmailinglijstenbyemail(email) {
      this.loading++;
      this.selectedemail = email;
      this.mailinglijstenbyemail.splice(0, this.mailinglijstenbyemail.length);
      try {
        let response = null;
        let nextPageToken = null;
        do {
          response = await gapi.client.directory.groups.list({
            domain: "biton.nl",
            fields: "groups(email,name),nextPageToken",
            userKey: email,
            pageToken: nextPageToken
          });
          console.log(response);
          if (response.result.groups)
            this.mailinglijstenbyemail.push(
              ...response.result.groups.map(({ email, name }) => ({
                Emailadres: email,
                Naam: name,
                Leden: []
              }))
            );
        } while (
          response.result.nextPageToken &&
          (nextPageToken = response.result.nextPageToken)
        );
        history.pushState(
          this.selectedmailinglijst,
          this.selectedmailinglijst ? this.selectedmailinglijst.Emailadres : "",
          "#" +
          (this.selectedmailinglijst
            ? encodeURIComponent(
              this.selectedmailinglijst.Emailadres.replace("@biton.nl", "")
            )
            : "")
        );
        setTimeout(() => {
          $("#byemailmodal")
            .modal("setting", "duration", 50)
            .modal("setting", "observeChanges", true)
            .modal({ inverted: true })
            .modal("show");
        }, 0);
      } catch (e) {
        console.error(e);
        if (e.result && e.result.error && e.result.error.message) {
          if (e.result.error.message === "Invalid Input: memberKey")
            alert("Ongeldig e-mailadres");
          else alert(e.result.error.message);
        } else {
          alert(
            "Er is een onbekende fout opgetreden. Ze de Developer Console voor meer informatie."
          );
        }
      } finally {
        this.loading--;
      }
    },
    async removemailinglijstfromlid(mailinglijst, lid) {
      this.loading++;
      await this.removelid(mailinglijst, lid);
      this.loadmailinglijstenbyemail(lid);
      this.loading--;
    }
  }
});
